# LNTXWebServiceHelper CHANGELOG

## 0.4.2

- **Fixed** `LNTXWebServiceCallConfiguration` to use `id` for parameters type instead of `NSDictionary`.

## 0.4.1
- **Added** A property on `LNTXWebServiceCallConfiguration` to accept all content types.

## 0.4.0

- **Added** `LNTXSessionWebServiceHelper`: An NSURLSessionTask based web service helper.

## 0.3.0

- **Updated** Perform JSON parsing / serialization on a background queue.
- **Added** A `completionQueue` property on `LNTXWebServiceCallConfiguration` to specify on what queue the completion block must be called.

## 0.2.0

- **Added** Support for HTTP PATCH based web services.
- **Added** Support for HTTP HEAD based web services.

## 0.1.6

- **Fixed** Parse response body even when an HTTP error occurs.

## 0.1.5

- **Fixed** Allow empty responses (HTTP 204) even when a parseable JSON content is expected.

## 0.1.4

- **Fixed** progress handlers for new AFNetworking version.

## 0.1.3

- **Fixed** web service completion bug when no result is required.

## 0.1.2

- **Fixed** general import header file.

## 0.1.1

- **Fixed** project directories structure.

## 0.1.0

Initial release. 

This release supports `NSData` and *OR-Mapped JSON* responses.

### Classes included

- `LNTXWebServiceHelper`: A generic protocol for creating a web service helper class.
- `LNTXRequestWebServiceHelper`: An `AFHTTPRequestOperation` based web service helper.
- `LNTXWebService`: A class used to define remote web services.
- `LNTXWebServiceCallConfiguration`: A class used to prepare a web service call.
- `LNTXRequest`: An abstract class that represent a currently executing request.
- `LNTXWebServiceRequest`: An `LNTXRequest` subclass that applies to `AFHTTPRequestOperation` objects.
- `LNTXJSONResponseSerializer`: A response serializer that can be passed to an `AFHTTPRequestOperation` to convert JSON data into data model objects.
- `LNTXWebServiceCompletion`: A set of classes used as web service helper completion handlers.