//
//  LNTXRequest.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXRequest.h"

@interface LNTXRequest ()

@property (nonatomic) NSString *requestID;
@property (nonatomic) NSDate *creationDate;
@property (nonatomic) NSMutableDictionary *userInfo;
@property (nonatomic) NSMutableArray *cancellables;

@end

@implementation LNTXRequest

#pragma mark - Initializers

- (id)init {
    if (self = [super init]) {
        self.creationDate = [NSDate date];
        
        self.cancellables = [NSMutableArray array];
        
        self.requestID = [[NSUUID UUID] UUIDString];
        
        self.userInfo = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - Associated callables

- (void)addCancellable:(id<LNTXCancellable>)cancellable {
    @synchronized (self) {
        if (![self isFinished] && cancellable) {
            if (![self isCancelled]) {
                [self.cancellables addObject:cancellable];
            } else {
                [cancellable cancel];
            }
        }
    }
}

- (void)removeCancellable:(id<LNTXCancellable>)cancellable {
    @synchronized (self) {
        [self.cancellables removeObject:cancellable];
    }
}

- (id<LNTXCancellable>)addCancellableWithCallback:(LNTXCancellableCallback)callback {
    id<LNTXCancellable> cancellable = [LNTXCancellable cancellableWithCallback:callback];
    [self addCancellableWithCallback:callback];
    
    return cancellable;
}

#pragma mark - State methods

- (BOOL)cancel {
    @synchronized (self) {
        if ([self isCancelled] || [self isFinished]) {
            return NO;
        }
        
        for (id<LNTXCancellable> cancellable in self.cancellables) {
            [cancellable cancel];
        }
        
        return YES;
    }
}

- (BOOL)pause {
    return NO;
}

- (BOOL)resume {
    return NO;
}

- (void)cleanUp {
    @synchronized (self) {
        [self.cancellables removeAllObjects];
    }
}

- (BOOL)isCancelled {
    return NO;
}

- (BOOL)isPaused {
    return NO;
}

- (BOOL)isFinished {
    return NO;
}

- (BOOL)isValid {
    return ![self isCancelled] && self.error == nil;
}

@end
