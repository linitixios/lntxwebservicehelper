//
//  LNTXSessionWebServiceHelper.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 08/11/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>
#import <LNTXWebServiceKit/LNTXWebServiceHelper.h>

@class LNTXWebServiceRequest;

/**
 @brief A helper class that ease the communication with remote web services through standard requests.
 Note that this class does uses the `NSURLSession` mechanism.
 */
@interface LNTXSessionWebServiceHelper : NSObject
<LNTXWebServiceHelper>

@property (readonly, nonatomic, strong) NSURL *baseURL;

- (instancetype)initWithBaseURL:(NSURL *)url;

@end
