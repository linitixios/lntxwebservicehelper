//
//  LNTXWebServiceRequest.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <LNTXWebServiceKit/LNTXRequest.h>

extern NSString * const LNTXRequestResponseObjectUserInfoKey;

@class LNTXWebServiceCallConfiguration;
@class AFHTTPRequestOperation;

@interface LNTXWebServiceRequest : LNTXRequest

+ (id)requestWithWebServiceCallConfiguration:(LNTXWebServiceCallConfiguration *)configuration;

@property (nonatomic, readonly) LNTXWebServiceCallConfiguration *webServiceCallConfiguration;

@property (nonatomic) AFHTTPRequestOperation *requestOperation; // Deprecated

@property (nonatomic) NSURLSessionDataTask *URLSessionDataTask;
@property (nonatomic, readonly) NSURLRequest *URLRequest;
@property (nonatomic, readonly) NSHTTPURLResponse *HTTPURLResponse;

@property (nonatomic) NSData *responseData;
@property (nonatomic) id responseObject;

@end
