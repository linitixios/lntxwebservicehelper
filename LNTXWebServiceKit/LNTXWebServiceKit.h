//
//  LNTXWebServiceKit.h
//  LNTXWebServiceKit
//
//  Created by Damien Rambout on 12/03/15.
//  Copyright (c) 2015 LINITIX. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for LNTXWebServiceKit.
FOUNDATION_EXPORT double LNTXWebServiceKitVersionNumber;

//! Project version string for LNTXWebServiceKit.
FOUNDATION_EXPORT const unsigned char LNTXWebServiceKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LNTXWebServiceKit/PublicHeader.h>

#import <LNTXWebServiceKit/LNTXRequestWebServiceHelper.h>
#import <LNTXWebServiceKit/LNTXSessionWebServiceHelper.h>
#import <LNTXWebServiceKit/LNTXWebService.h>
#import <LNTXWebServiceKit/LNTXWebServiceCallConfiguration.h>
#import <LNTXWebServiceKit/LNTXRequest.h>
#import <LNTXWebServiceKit/LNTXWebServiceRequest.h>
#import <LNTXWebServiceKit/LNTXCancellable.h>
#import <LNTXWebServiceKit/LNTXWebServiceCompletion.h>
