//
//  LNTXWebServiceRequest.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXWebServiceRequest.h"

#import <AFNetworking/AFHTTPRequestOperation.h>

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

NSString * const LNTXRequestResponseObjectUserInfoKey = @"LNTXRequestResponseObjectUserInfoKey";

@interface LNTXWebServiceRequest ()

@property (nonatomic) LNTXWebServiceCallConfiguration *webServiceCallConfiguration;

@end

@implementation LNTXWebServiceRequest

+ (id)requestWithWebServiceCallConfiguration:(LNTXWebServiceCallConfiguration *)configuration {
    NSParameterAssert(configuration);
    
    LNTXWebServiceRequest *request = [[self alloc] init];
    request.webServiceCallConfiguration = configuration;
    
    return request;
}

#pragma mark - NSObject

LNTXDefineAutoDescription();

#pragma mark - Accessors

- (NSURLRequest *)URLRequest {
    if (self.requestOperation) {
        return self.requestOperation.request;
    }
    
    return self.URLSessionDataTask.currentRequest;
}

- (NSHTTPURLResponse *)HTTPURLResponse {
    if (self.requestOperation) {
        return self.requestOperation.response;
    }
    
    return (NSHTTPURLResponse *)self.URLSessionDataTask.response;
}

#pragma mark - State update

- (BOOL)cancel {
    if (![self isCancelled]) {
        [self.requestOperation cancel];
        return [super cancel];
    }
    
    return NO;
}

- (BOOL)pause {
    if (![self isPaused]) {
        [self.requestOperation pause];
        return YES;
    }
    
    return NO;
}

- (BOOL)resume {
    if ([self isPaused]) {
        [self.requestOperation resume];
        return YES;
    }
    
    return NO;
}

#pragma mark - State read

- (BOOL)isCancelled {
    return [self.requestOperation isCancelled];
}

- (BOOL)isPaused {
    return [self.requestOperation isPaused];
}

- (BOOL)isFinished {
    return [self.requestOperation isFinished];
}

@end
