//
//  LNTXRequestWebServiceHelper.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 19/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <LNTXWebServiceKit/LNTXWebServiceHelper.h>

@class LNTXWebServiceRequest;

/**
 @brief A helper class that ease the communication with remote web services through standard requests.
 Note that this class does not use the `NSURLSession` mechanism but the `NSURLConnection` one.
 */
@interface LNTXRequestWebServiceHelper : AFHTTPRequestOperationManager
<LNTXWebServiceHelper>

@end
