//
//  LNTXCancellable.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LNTXCancellableCallback)();

@protocol LNTXCancellable <NSObject>

/**
 * @brief Cancels the current object.
 * @return YES if the task was canceled, NO otherwise.
 */
- (BOOL)cancel;

@end

@interface LNTXCancellable : NSObject
<LNTXCancellable>

/**
 @brief Creates and returns an object that will call the provided callback when canceled.
 @return An object that will call the provided callback when canceled.
 */
+ (id<LNTXCancellable>)cancellableWithCallback:(LNTXCancellableCallback)callback;

/**
 @brief Creates and returns an object that will call the provided callback when canceled.
 @return An object that will call the provided callback when canceled.
 */
- (id)initWithCallback:(LNTXCancellableCallback)callback;

@end
