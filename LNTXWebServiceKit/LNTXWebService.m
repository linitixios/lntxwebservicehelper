//
//  LNTXWebService.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXWebService.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>

NSString *NSStringFromHTTPMethod(LNTXHTTPMethod HTTPMethod) {
    switch (HTTPMethod) {
        case LNTXHTTPMethodGET:
            return @"GET";
        case LNTXHTTPMethodPOST:
            return @"POST";
        case LNTXHTTPMethodPUT:
            return @"PUT";
        case LNTXHTTPMethodDELETE:
            return @"DELETE";
        case LNTXHTTPMethodPATCH:
            return @"PATCH";
        case LNTXHTTPMethodHEAD:
            return @"HEAD";
    }
}

NSString * const LNTXContentTypeJSON = @"application/json";
NSString * const LNTXContentTypeFormURLEncoded = @"application/x-www-form-urlencoded";

LNTXWebService *LNTXWebServiceMake(NSString *path,
                                   LNTXHTTPMethod HTTPMethod,
                                   NSString *requestContentType,
                                   NSString *responseContentType,
                                   id<LNTXParserConfiguration>parserConfiguration) {
    return [[[[[[[LNTXWebServiceBuilder builder]
                 setPath:path]
                setHTTPMethod:HTTPMethod]
               setRequestContentType:requestContentType]
              setResponseContentType:responseContentType]
             setParserConfiguration:parserConfiguration]
            build];
}

extern LNTXWebService *LNTXWebServiceMakeGET(NSString *path,
                                             NSString *responseContentType,
                                             id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodGET, nil, responseContentType, parserConfiguration);
}

extern LNTXWebService *LNTXWebServiceMakePOST(NSString *path,
                                              NSString *requestContentType,
                                              NSString *responseContentType,
                                              id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodPOST, requestContentType, responseContentType, parserConfiguration);
}

extern LNTXWebService *LNTXWebServiceMakePUT(NSString *path,
                                             NSString *requestContentType,
                                             NSString *responseContentType,
                                             id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodPUT, requestContentType, responseContentType, parserConfiguration);
}

extern LNTXWebService *LNTXWebServiceMakeDELETE(NSString *path,
                                                NSString *responseContentType,
                                                id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodDELETE, nil, responseContentType, parserConfiguration);
}

extern LNTXWebService *LNTXWebServiceMakePATCH(NSString *path,
                                               NSString *requestContentType,
                                               NSString *responseContentType,
                                               id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodPATCH, requestContentType, responseContentType, parserConfiguration);
}

extern LNTXWebService *LNTXWebServiceMakeHEAD(NSString *path,
                                              id<LNTXParserConfiguration>parserConfiguration) {
    return LNTXWebServiceMake(path, LNTXHTTPMethodHEAD, nil, nil, parserConfiguration);
}

@interface LNTXWebService ()

@property (nonatomic, copy) NSString *path;
@property (nonatomic) LNTXHTTPMethod HTTPMethod;
@property (nonatomic, copy) NSString *requestContentType;
@property (nonatomic, copy) NSString *responseContentType;
@property (nonatomic) id<LNTXParserConfiguration>parserConfiguration;

@end

@implementation LNTXWebService

#pragma mark - Initialization

- (id)initWithWebService:(LNTXWebService *)webService
{
    if (self = [super init]) {
        self.path = webService.path;
        self.HTTPMethod = webService.HTTPMethod;
        self.requestContentType = webService.requestContentType;
        self.responseContentType = webService.responseContentType;
        self.parserConfiguration = webService.parserConfiguration;
    }
    
    return self;
}

- (NSString *)pathByApplyingFormatValues:(NSDictionary *)formatValues {
    if (!formatValues) {
        return self.path;
    }
    
    NSMutableString *mutablePath = [self.path mutableCopy];
    
    [formatValues enumerateKeysAndObjectsUsingBlock:^(NSString *key,
                                                      NSString *obj,
                                                      BOOL *stop) {
        [mutablePath replaceOccurrencesOfString:[NSString stringWithFormat:@"{%@}", key]
                                     withString:obj
                                        options:0
                                          range:NSMakeRange(0, mutablePath.length)];
    }];
    
    return [mutablePath copy];
}

#pragma mark - NSObject

LNTXDefineAutoDescription();

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:self.class]) {
        return NO;
    }
    
    LNTXWebService *webService = object;
    
    return ([self.path isEqualToString:webService.path] &&
            self.HTTPMethod == webService.HTTPMethod);
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    return [[LNTXWebService alloc] initWithWebService:self];
}

#pragma mark - NSMutableCopying

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [[LNTXMutableWebService alloc] initWithWebService:self];
}

@end

@implementation LNTXMutableWebService

@dynamic path, HTTPMethod, parserConfiguration, requestContentType, responseContentType;

@end

// =================================================================
// =================================================================
// =================================================================

@interface LNTXWebServiceBuilder ()

@property (nonatomic) LNTXMutableWebService *webService;

@end

@implementation LNTXWebServiceBuilder

+ (instancetype)builder {
    return [[self alloc] init];
}

- (id)init
{
    if (self = [super init]) {
        self.webService = [[LNTXMutableWebService alloc] init];
    }
    
    return self;
}

- (LNTXWebService *)build {
    return [self.webService copy];
}

- (instancetype)setPath:(NSString *)path {
    self.webService.path = path;
    return self;
}

- (instancetype)setHTTPMethod:(LNTXHTTPMethod)HTTPMethod {
    self.webService.HTTPMethod = HTTPMethod;
    return self;
}

- (instancetype)setRequestContentType:(NSString *)requestContentType {
    self.webService.requestContentType = requestContentType;
    return self;
}

- (instancetype)setResponseContentType:(NSString *)responseContentType {
    self.webService.responseContentType = responseContentType;
    return self;
}

- (instancetype)setParserConfiguration:(id<LNTXParserConfiguration>)parserConfiguration {
    self.webService.parserConfiguration = parserConfiguration;
    return self;
}

@end
