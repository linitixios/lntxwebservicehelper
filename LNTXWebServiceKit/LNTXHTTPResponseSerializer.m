//
//  LNTXHTTPResponseSerializer.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 08/11/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXHTTPResponseSerializer.h"

#import "LNTXWebServiceRequest.h"

@implementation LNTXHTTPResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error {
    id responseObject = [super responseObjectForResponse:response data:data error:error];
    
    if (*error && responseObject) {
        // Add response object in error user info
        NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
        userInfo[LNTXRequestResponseObjectUserInfoKey] = responseObject;
        *error = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
    }
    
    return responseObject;
}

@end
