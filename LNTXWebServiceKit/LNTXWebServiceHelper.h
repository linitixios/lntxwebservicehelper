//
//  LNTXWebServiceHelper.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LNTXWebServiceRequest;
@class LNTXWebServiceCallConfiguration;
@class LNTXWebServiceCompletion;

/**
 @brief A generic protocol for defining a class that will ease the communication with remote web services.
 */
@protocol LNTXWebServiceHelper <NSObject>

/**
 @brief Calls a remote web service based on the provided configuration and returns a `NSData` object through the 
 completion object.
 @param configuration A configuration for the web service to call.
 @param configuration A completion object through which data or error will be returned.
 @return An object that represents the web service request.
 */
- (LNTXWebServiceRequest *)performHTTPCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                                 completion:(LNTXWebServiceCompletion *)completion;

/**
 @brief Calls a remote web service based on the provided configuration and returns an OR-mapped object based on the JSON 
 web service response through the completion object.
 @param configuration A configuration for the web service to call.
 @param configuration A completion object through which the OR-mapped object or an error will be returned.
 @return An object that represents the web service request.
 */
- (LNTXWebServiceRequest *)performJSONCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                                 completion:(LNTXWebServiceCompletion *)completion;

@end
