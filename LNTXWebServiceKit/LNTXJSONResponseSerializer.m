//
//  LNTXJSONResponseSerializer.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXJSONResponseSerializer.h"

#import <LNTXParser/LNTXJSONParser.h>
#import "LNTXWebServiceRequest.h"

@interface LNTXJSONResponseSerializer ()

@property (nonatomic) id<LNTXParserConfiguration>parserConfiguration;

@end

@implementation LNTXJSONResponseSerializer

+ (instancetype)serializerWithParserConfiguration:(id<LNTXParserConfiguration>)parserConfiguration {
    NSParameterAssert(parserConfiguration);
    
    LNTXJSONResponseSerializer *serializer = [self serializerWithReadingOptions:0];
    serializer.parserConfiguration = parserConfiguration;
    
    return serializer;
}

- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error {
    id jsonObject = [super responseObjectForResponse:response data:data error:error];
    
    if (!jsonObject) {
        // Add response in error user info
        LNTXJSONResponseObject *returnValue = [[LNTXJSONResponseObject alloc] initWithData:data object:nil];
        *error = [self errorWithBaseError:*error additionalUserInfo:@{LNTXRequestResponseObjectUserInfoKey : returnValue}];
        return returnValue;
    }
    
    // Parse JSON into data model
    LNTXJSONParser *parser = [[LNTXJSONParser alloc] initWithConfiguration:self.parserConfiguration];
    id responseObject = [parser parseObject:jsonObject error:error];
    LNTXJSONResponseObject *returnValue = [[LNTXJSONResponseObject alloc] initWithData:data
                                                                                object:responseObject];
    
    if (*error) {
        // Add response object in error user info
        *error = [self errorWithBaseError:*error additionalUserInfo:@{LNTXRequestResponseObjectUserInfoKey: returnValue}];
    }
    
    return [[LNTXJSONResponseObject alloc] initWithData:data object:responseObject];
}

- (NSError *)errorWithBaseError:(NSError *)error additionalUserInfo:(NSDictionary *)additionalUserInfo {
    if (!error) {
        return nil;
    }
    
    NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
    [userInfo setValuesForKeysWithDictionary:additionalUserInfo];
    
    return [NSError errorWithDomain:error.domain code:error.code userInfo:userInfo];
}

@end

@implementation LNTXJSONResponseObject

- (instancetype)initWithData:(NSData *)data object:(id)object {
    if (self = [super init]) {
        _data = data;
        _object = object;
    }
    
    return self;
}

@end
