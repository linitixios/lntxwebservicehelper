//
//  LNTXRequestWebServiceHelper.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 19/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXRequestWebServiceHelper.h"

#import "LNTXJSONResponseSerializer.h"
#import "LNTXHTTPResponseSerializer.h"
#import "LNTXWebServiceCallConfiguration.h"
#import "LNTXWebService.h"
#import "LNTXWebServiceCompletion.h"
#import "LNTXWebServiceRequest.h"
#import <AFNetworking/AFHTTPRequestOperation.h>

static NSString * const kUnsupportedContentTypeException = @"LNTXUnsupportedContentTypeException";

typedef void(^LNTXOperationCompletion)(LNTXWebServiceRequest *request, id responseObject, NSError *error);
typedef void(^AFHTTPRequestOperationSuccess)(AFHTTPRequestOperation *operation, id responseObject);
typedef void(^AFHTTPRequestOperationFailure)(AFHTTPRequestOperation *operation, NSError *error);

@interface LNTXRequestWebServiceHelper ()

static AFHTTPRequestOperationSuccess completionToSuccess(LNTXOperationCompletion completion, LNTXWebServiceRequest *request);
static AFHTTPRequestOperationFailure completionToFailure(LNTXOperationCompletion completion, LNTXWebServiceRequest *request);

@end

@implementation LNTXRequestWebServiceHelper

- (id)init
{
    return [self initWithBaseURL:nil];
}

- (LNTXWebServiceRequest *)performHTTPCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                                 completion:(LNTXWebServiceCompletion *)completion {
    LNTXWebServiceRequest *request = [self performCallWithConfiguration:configuration
                                                             completion:^(LNTXWebServiceRequest *request,
                                                                          id responseObject,
                                                                          NSError *error) {
                                                                 request.responseData = responseObject;
                                                                 request.responseObject = responseObject;
                                                                 [completion handleResult:responseObject request:request];
                                                             }];
    
    // Prepare for response (simple data)
    request.requestOperation.responseSerializer = [LNTXHTTPResponseSerializer serializer];
    
    request.requestOperation.completionQueue = configuration.completionQueue;
    request.requestOperation.completionGroup = configuration.completionGroup;
    
    return request;
}

- (LNTXWebServiceRequest *)performJSONCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                                 completion:(LNTXWebServiceCompletion *)completion {
    NSAssert(configuration.webService.parserConfiguration != nil,
             @"No parser configuration defined for web service %@. Define one to fetch JSON data.",
             configuration.webService);
    
    LNTXWebServiceRequest *request = [self performCallWithConfiguration:configuration
                                                             completion:^(LNTXWebServiceRequest *request,
                                                                          LNTXJSONResponseObject *responseObject,
                                                                          NSError *error) {
                                                                 request.responseData = responseObject.data;
                                                                 request.responseObject = responseObject.object;
                                                                 [completion handleResult:responseObject.object request:request];
                                                             }];
    
    // Prepare for response (json parser)
    id <LNTXParserConfiguration> parserConfiguration = configuration.webService.parserConfiguration;
    request.requestOperation.responseSerializer = [LNTXJSONResponseSerializer serializerWithParserConfiguration:parserConfiguration];
    
    if (configuration.shouldAcceptAllContentTypes) {
        request.requestOperation.responseSerializer.acceptableContentTypes = nil;
    }
    
    request.requestOperation.completionQueue = configuration.completionQueue;
    request.requestOperation.completionGroup = configuration.completionGroup;
    
    return request;
}

- (LNTXWebServiceRequest *)performCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                             completion:(LNTXOperationCompletion)completion {
    LNTXWebServiceRequest *request = [LNTXWebServiceRequest requestWithWebServiceCallConfiguration:configuration];
    AFHTTPRequestOperation *operation = nil;
    
    // Prepare manager for request
    self.requestSerializer = [self.class requestSerializerForContentType:configuration.webService.requestContentType];
    
    // Set HTTP headers
    [configuration.HTTPHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [self.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    
    NSString *path = [configuration formattedWebServicePath];
    
    switch (configuration.webService.HTTPMethod) {
        case LNTXHTTPMethodGET: {
            // GET
            operation = [self GET:path
                       parameters:configuration.parameters
                          success:completionToSuccess(completion, request)
                          failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodPOST: {
            // POST
            if (configuration.bodyConstructionBlock) {
                // Multipart
                operation = [self POST:path
                            parameters:configuration.parameters
             constructingBodyWithBlock:configuration.bodyConstructionBlock
                               success:completionToSuccess(completion, request)
                               failure:completionToFailure(completion, request)];
            } else {
                // Regular
                operation = [self POST:path
                            parameters:configuration.parameters
                               success:completionToSuccess(completion, request)
                               failure:completionToFailure(completion, request)];
            }
            
            break;
        }
            
        case LNTXHTTPMethodPUT: {
            // PUT
            operation = [self PUT:path
                       parameters:configuration.parameters
                          success:completionToSuccess(completion, request)
                          failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodDELETE: {
            // DELETE
            operation = [self DELETE:path
                          parameters:configuration.parameters
                             success:completionToSuccess(completion, request)
                             failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodPATCH: {
            // PATCH
            operation = [self PATCH:path
                         parameters:configuration.parameters
                            success:completionToSuccess(completion, request)
                            failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodHEAD: {
            // HEAD
            operation = [self HEAD:path
                        parameters:configuration.parameters
                           success:^(AFHTTPRequestOperation *operation) {
                               completion(request, nil, nil);
                           }
                           failure:completionToFailure(completion, request)];
        }
    }
    
    request.requestOperation = operation;
    
    // Download progress
    if (configuration.downloadProgressBlock) {
        [operation setDownloadProgressBlock:configuration.downloadProgressBlock];
    }
    
    // Upload progress
    if (configuration.uploadProgressBlock) {
        [operation setUploadProgressBlock:configuration.uploadProgressBlock];
    }
    
    return request;
}

+ (AFHTTPRequestSerializer *)requestSerializerForContentType:(NSString *)contentType {
    if ([contentType isEqualToString:LNTXContentTypeJSON]) {
        return [AFJSONRequestSerializer serializer];
    }
    
    if (!contentType ||
        [contentType isEqualToString:LNTXContentTypeFormURLEncoded] ||
        [contentType hasPrefix:@"multipart/"]) {
        return [AFHTTPRequestSerializer serializer];
    }
    
    [NSException raise:kUnsupportedContentTypeException
                format:@"Content-type '%@' is not support supported by class %@.", contentType, NSStringFromClass(self.class)];
    return nil;
}

#pragma mark - Helper methods

static AFHTTPRequestOperationSuccess completionToSuccess(LNTXOperationCompletion completion, LNTXWebServiceRequest *request) {
    return ^(AFHTTPRequestOperation *operation, id responseObject) {
        completion(request, responseObject, nil);
    };
}

static AFHTTPRequestOperationFailure completionToFailure(LNTXOperationCompletion completion, LNTXWebServiceRequest *request) {
    return ^(AFHTTPRequestOperation *operation, NSError *error) {
        request.error = error;
        completion(request, operation.responseObject, error);
    };
}

@end
