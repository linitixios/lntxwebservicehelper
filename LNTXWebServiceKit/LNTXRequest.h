//
//  LNTXRequest.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <LNTXWebServiceKit/LNTXCancellable.h>

@interface LNTXRequest : NSObject
<LNTXCancellable>

/** A unique id. */
@property (nonatomic, readonly) NSString *requestID;
/** The date at which the request was created. */
@property (nonatomic, readonly) NSDate *creationDate;
/** Tells whether the request was canceled. */
@property (nonatomic, readonly, getter = isCancelled) BOOL cancelled;
@property (nonatomic, readonly, getter = isPaused) BOOL paused;
/** Tells whether the request has finished. */
@property (nonatomic, readonly, getter = isFinished) BOOL finished;
/** Tells whether the request was canceled. */
@property (nonatomic, strong) NSError *error;
/** A mutable dictionary that lets the user set custom information. */
@property (nonatomic, readonly) NSMutableDictionary *userInfo;

- (void)addCancellable:(id<LNTXCancellable>)cancellable;
- (void)removeCancellable:(id<LNTXCancellable>)cancellable;
- (id<LNTXCancellable>)addCancellableWithCallback:(LNTXCancellableCallback)callback;

- (BOOL)cancel;
- (BOOL)pause;
- (BOOL)resume;

/** 
 Removes all cancellable callbacks from internals.
 */
- (void)cleanUp;

/**
 @return YES if the request is neither canceled nor contains an error, NO otherwise.
 */
- (BOOL)isValid;

@end
