//
//  LNTXWebServiceCompletion.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LNTXWebServiceRequest;

@interface LNTXWebServiceCompletion : NSObject

+ (id)completionWithBlock:(void(^)(id result, LNTXWebServiceRequest *request))block;
- (void)handleResult:(id)result
             request:(LNTXWebServiceRequest *)request;

@end

@interface LNTXWebServiceNoResultCompletion : LNTXWebServiceCompletion

+ (id)completionWithBlock:(void(^)(LNTXWebServiceRequest *request))block;

@end

@interface LNTXWebServiceEmptyCompletion : LNTXWebServiceNoResultCompletion

+ (id)completion;

@end
