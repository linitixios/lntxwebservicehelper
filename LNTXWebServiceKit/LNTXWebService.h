//
//  LNTXWebService.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LNTXParserConfiguration;
@class LNTXWebService;

typedef NS_ENUM(NSUInteger, LNTXHTTPMethod) {
    /** HTTP GET method. */
    LNTXHTTPMethodGET,
    /** HTTP POST method. */
    LNTXHTTPMethodPOST,
    /** HTTP PUT method. */
    LNTXHTTPMethodPUT,
    /** HTTP DELETE method. */
    LNTXHTTPMethodDELETE,
    /** HTTP PATCH method */
    LNTXHTTPMethodPATCH,
    /** HTTP HEAD method */
    LNTXHTTPMethodHEAD
};

/** Media type for JSON. */
extern NSString * const LNTXContentTypeJSON;
/** Media type for Form Encoded Data */
extern NSString * const LNTXContentTypeFormURLEncoded;

extern NSString *NSStringFromHTTPMethod(LNTXHTTPMethod HTTPMethod);

extern LNTXWebService *LNTXWebServiceMake(NSString *path,
                                          LNTXHTTPMethod HTTPMethod,
                                          NSString *requestContentType,
                                          NSString *responseContentType,
                                          id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakeGET(NSString *path,
                                             NSString *responseContentType,
                                             id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakePOST(NSString *path,
                                              NSString *requestContentType,
                                              NSString *responseContentType,
                                              id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakePUT(NSString *path,
                                             NSString *requestContentType,
                                             NSString *responseContentType,
                                             id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakeDELETE(NSString *path,
                                                NSString *responseContentType,
                                                id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakePATCH(NSString *path,
                                               NSString *requestContentType,
                                               NSString *responseContentType,
                                               id<LNTXParserConfiguration>parserConfiguration);

extern LNTXWebService *LNTXWebServiceMakeHEAD(NSString *path,
                                              id<LNTXParserConfiguration>parserConfiguration);

/**
 Defines a web service.
 */
@interface LNTXWebService : NSObject
<NSCopying, NSMutableCopying>

/** A relative path for the web service. */
@property (nonatomic, readonly, copy) NSString *path;
/** The HTTP method to use for this web service. */
@property (nonatomic, readonly) LNTXHTTPMethod HTTPMethod;
/** A Content-Type for the HTTP request. */
@property (nonatomic, readonly, copy) NSString *requestContentType;
/** A required Content-Type for the HTTP response. */
@property (nonatomic, readonly, copy) NSString *responseContentType;
/** An optional configuration for parsing response data. */
@property (nonatomic, readonly) id<LNTXParserConfiguration>parserConfiguration;

/**
 @brief Returns the web service path in which format keys have been replaced with provided values. A path format key has the
 form '{key}', where 'key' is the name of the key to replace with a value. The `formatValues` dictionary keys must not 
 include the brackets, simply the key names.
 
 For instance if a web service's path is 'users/{id}/friends', a valid format values dictionary could be
 {@"id": @"123"}, to place '{id}' with '123'.
 
 @param formatValues The values mapped to keys to be replaced in the web service's path.
 
 @return The web service path in which format keys have been replaced with provided values.
 */
- (NSString *)pathByApplyingFormatValues:(NSDictionary *)formatValues;

@end

/**
 A mutable version of a web service definition.
 */
@interface LNTXMutableWebService : LNTXWebService

@property (nonatomic, copy) NSString *path;
@property (nonatomic) LNTXHTTPMethod HTTPMethod;
@property (nonatomic, copy) NSString *requestContentType;
@property (nonatomic, copy) NSString *responseContentType;
@property (nonatomic) id<LNTXParserConfiguration>parserConfiguration;

@end

/**
 A builder to easily create a web service definition.
 */
@interface LNTXWebServiceBuilder : NSObject

+ (instancetype)builder;
- (LNTXWebService *)build;
- (instancetype)setPath:(NSString *)path;
- (instancetype)setHTTPMethod:(LNTXHTTPMethod)HTTPMethod;
- (instancetype)setRequestContentType:(NSString *)requestContentType;
- (instancetype)setResponseContentType:(NSString *)responseContentType;
- (instancetype)setParserConfiguration:(id<LNTXParserConfiguration>)parserConfiguration;

@end
