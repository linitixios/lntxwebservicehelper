//
//  LNTXHTTPResponseSerializer.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 08/11/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <AFNetworking/AFURLResponseSerialization.h>

@interface LNTXHTTPResponseSerializer : AFHTTPResponseSerializer

@end
