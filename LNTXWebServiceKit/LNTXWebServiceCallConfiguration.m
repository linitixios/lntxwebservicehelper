//
//  LNTXWebServiceCallConfiguration.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXWebServiceCallConfiguration.h"

#import <LNTXCoreKit/NSObject+LNTXAutoDescription.h>
#import "LNTXWebService.h"

@interface LNTXWebServiceCallConfiguration ()

@property (nonatomic, copy) LNTXWebService *webService;
@property (nonatomic, copy) NSDictionary *pathFormatValues;
@property (nonatomic, copy) id parameters;
@property (nonatomic, copy) NSDictionary *HTTPHeaders;
@property (nonatomic, copy) LNTXDownloadProgressBlock downloadProgressBlock;
@property (nonatomic, copy) LNTXUploadProgressBlock uploadProgressBlock;
@property (nonatomic, copy) LNTXBodyConstructionBlock bodyConstructionBlock;
@property (nonatomic) dispatch_queue_t completionQueue;
@property (nonatomic) dispatch_group_t completionGroup;
@property (nonatomic) NSURLSessionConfiguration *sessionConfiguration;
@property (nonatomic) BOOL shouldAcceptAllContentTypes;

@end

@implementation LNTXWebServiceCallConfiguration

#pragma mark - Initialization

- (id)initWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
{
    if (self = [super init]) {
        self.webService = configuration.webService;
        self.pathFormatValues = configuration.pathFormatValues;
        self.parameters = configuration.parameters;
        self.HTTPHeaders = configuration.HTTPHeaders;
        self.downloadProgressBlock = configuration.downloadProgressBlock;
        self.uploadProgressBlock = configuration.uploadProgressBlock;
        self.bodyConstructionBlock = configuration.bodyConstructionBlock;
        self.completionQueue = configuration.completionQueue;
        self.completionGroup = configuration.completionGroup;
        self.sessionConfiguration = configuration.sessionConfiguration;
        self.shouldAcceptAllContentTypes = configuration.shouldAcceptAllContentTypes;
    }
    
    return self;
}

- (NSString *)formattedWebServicePath {
    return [self.webService pathByApplyingFormatValues:self.pathFormatValues];
}

#pragma mark - NSObject

LNTXDefineAutoDescription();

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    return [[LNTXWebServiceCallConfiguration alloc] initWithConfiguration:self];
}

#pragma mark - NSMutableCopying

- (id)mutableCopyWithZone:(NSZone *)zone {
    return [[LNTXMutableWebServiceCallConfiguration alloc] initWithConfiguration:self];
}

@end

@implementation LNTXMutableWebServiceCallConfiguration

@dynamic webService, HTTPHeaders, pathFormatValues, parameters, downloadProgressBlock, uploadProgressBlock, bodyConstructionBlock, completionQueue, completionGroup, sessionConfiguration, shouldAcceptAllContentTypes;

@end

@interface LNTXWebServiceCallConfigurationBuilder ()

@property (nonatomic) LNTXMutableWebServiceCallConfiguration *configuration;

@end

@implementation LNTXWebServiceCallConfigurationBuilder

+ (instancetype)builder {
    return [[LNTXWebServiceCallConfigurationBuilder alloc] init];
}

- (id)init
{
    if (self = [super init]) {
        self.configuration = [[LNTXMutableWebServiceCallConfiguration alloc] init];
    }
    
    return self;
}

- (LNTXWebServiceCallConfiguration *)build {
    return [self.configuration copy];
}

- (instancetype)setWebService:(LNTXWebService *)webService {
    self.configuration.webService = webService;
    return self;
}

- (instancetype)setPathFormatValues:(NSDictionary *)pathFormatValues {
    self.configuration.pathFormatValues = pathFormatValues;
    return self;
}

- (instancetype)setParameters:(id)parameters {
    self.configuration.parameters = parameters;
    return self;
}

- (instancetype)setHTTPHeaders:(NSDictionary *)HTTPHeaders {
    self.configuration.HTTPHeaders = HTTPHeaders;
    return self;
}

- (instancetype)setDownloadProgressBlock:(LNTXDownloadProgressBlock)downloadProgressBlock {
    self.configuration.downloadProgressBlock = downloadProgressBlock;
    return self;
}

- (instancetype)setUploadProgressBlock:(LNTXUploadProgressBlock)uploadProgressBlock {
    self.configuration.uploadProgressBlock = uploadProgressBlock;
    return self;
}

- (instancetype)setBodyConstructionBlock:(LNTXBodyConstructionBlock)bodyConstructionBlock {
    self.configuration.bodyConstructionBlock = bodyConstructionBlock;
    return self;
}

- (instancetype)setCompletionQueue:(dispatch_queue_t)completionQueue {
    self.configuration.completionQueue = completionQueue;
    return self;
}

- (instancetype)setCompletionGroup:(dispatch_group_t)completionGroup {
    self.configuration.completionGroup = completionGroup;
    return self;
}

- (instancetype)setSessionConfiguration:(NSURLSessionConfiguration *)configuration {
    self.configuration.sessionConfiguration = configuration;
    return self;
}

- (instancetype)setShouldAcceptAllContentTypes:(BOOL)shouldAcceptAllContentTypes {
    self.configuration.shouldAcceptAllContentTypes = shouldAcceptAllContentTypes;
    return self;
}

@end
