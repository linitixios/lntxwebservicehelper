//
//  LNTXJSONResponseSerializer.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <AFNetworking/AFURLResponseSerialization.h>

@protocol LNTXParserConfiguration;

@interface LNTXJSONResponseSerializer : AFJSONResponseSerializer

+ (instancetype)serializerWithParserConfiguration:(id<LNTXParserConfiguration>)parserConfiguration;

@property (nonatomic, readonly) id<LNTXParserConfiguration>parserConfiguration;

@end

@interface LNTXJSONResponseObject : NSObject

- (instancetype)initWithData:(NSData *)data object:(id)object;

@property (nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) id object;

@end
