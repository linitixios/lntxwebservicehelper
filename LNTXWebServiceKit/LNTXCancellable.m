//
//  LNTXCancellable.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 17/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXCancellable.h"

@interface LNTXCancellable ()

@property (nonatomic, copy) LNTXCancellableCallback callback;

@end

@implementation LNTXCancellable

+ (id<LNTXCancellable>)cancellableWithCallback:(LNTXCancellableCallback)callback
{
    return [[self alloc] initWithCallback:callback];
}

- (id)initWithCallback:(LNTXCancellableCallback)callback
{
    if (self = [super init])
    {
        self.callback = callback;
    }
    
    return self;
}

- (BOOL)cancel
{
    if (self.callback)
    {
        self.callback();
    }
    
    return YES;
}

@end
