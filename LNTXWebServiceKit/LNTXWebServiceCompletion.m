//
//  LNTXWebServiceCompletion.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXWebServiceCompletion.h"

#import "LNTXWebServiceRequest.h"

@interface LNTXWebServiceCompletion ()

@property (nonatomic, copy) void(^resultCompletion)(id result, LNTXWebServiceRequest *request);

@end

@implementation LNTXWebServiceCompletion

+ (id)completionWithBlock:(void(^)(id result, LNTXWebServiceRequest *request))block
{
    LNTXWebServiceCompletion *completion = [[LNTXWebServiceCompletion alloc] init];
    completion.resultCompletion = block;
    
    return completion;
}

- (void)handleResult:(id)result request:(LNTXWebServiceRequest *)request
{
    if (self.resultCompletion)
    {
        self.resultCompletion(result, request);
    }
}

@end

@implementation LNTXWebServiceNoResultCompletion

+ (id)completionWithBlock:(void(^)(LNTXWebServiceRequest *request))block {
    LNTXWebServiceNoResultCompletion *completion = [[LNTXWebServiceNoResultCompletion alloc] init];
    completion.resultCompletion = ^(id result, LNTXWebServiceRequest *request) {
        block(request);
    };
    
    return completion;
}

@end

@implementation LNTXWebServiceEmptyCompletion

+ (id)completion
{
    return [[self alloc] init];
}

- (void)handleResult:(id)result request:(LNTXWebServiceRequest *)request
{
    // Do nothing
}

@end
