//
//  LNTXWebServiceCallConfiguration.h
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 18/02/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LNTXWebService;
@protocol AFMultipartFormData;

typedef void(^LNTXDownloadProgressBlock)(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead);
typedef void(^LNTXUploadProgressBlock)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite);
typedef void(^LNTXBodyConstructionBlock)(id<AFMultipartFormData> formData);

/**
 @brief A configuration for calling a remote web service.
 */
@interface LNTXWebServiceCallConfiguration : NSObject
<NSCopying, NSMutableCopying>

/** The remote web service to call. */
@property (nonatomic, readonly, copy) LNTXWebService *webService;
/** A mapping of values to replace format keys in the web service path.  */
@property (nonatomic, readonly, copy) NSDictionary *pathFormatValues;
/**
 Parameters to pass to the web service.
 @note These will be used as URL parameters for GET and DELETE calls.
 @note These will be used as body parameters for POST and PUT calls.
 */
@property (nonatomic, readonly, copy) id parameters;
/**
 HTTP headers to pass to the HTTP request.
 */
@property (nonatomic, readonly, copy) NSDictionary *HTTPHeaders;
/**
 A download progress block that will be called back when bytes are received.
 */
@property (nonatomic, readonly, copy) LNTXDownloadProgressBlock downloadProgressBlock;
/**
 An upload progress block that will be called back when bytes are sent.
 */
@property (nonatomic, readonly, copy) LNTXUploadProgressBlock uploadProgressBlock;
/**
 A block used to construct a multipart content body. Only used for POST requests.
 */
@property (nonatomic, readonly, copy) LNTXBodyConstructionBlock bodyConstructionBlock;

/**
 The dispatch queue on which the completion block is called. If `NULL` (default), the main queue is used.
 */
@property (nonatomic, readonly) dispatch_queue_t completionQueue;

/**
 The dispatch group for `completionBlock`. If `NULL` (default), a private dispatch group is used.
 */
@property (nonatomic, readonly) dispatch_group_t completionGroup;

@property (nonatomic, readonly) NSURLSessionConfiguration *sessionConfiguration;

@property (nonatomic, readonly) BOOL shouldAcceptAllContentTypes;

/**
 Returns a fully formatted path for the related web service. All path format values are applied to the path returned.
 @return A fully formatted path for the related web service.
 */
- (NSString *)formattedWebServicePath;

@end

@interface LNTXMutableWebServiceCallConfiguration : LNTXWebServiceCallConfiguration

@property (nonatomic, copy) LNTXWebService *webService;
@property (nonatomic, copy) NSDictionary *pathFormatValues;
@property (nonatomic, copy) id parameters;
@property (nonatomic, copy) NSDictionary *HTTPHeaders;
@property (nonatomic, copy) LNTXDownloadProgressBlock downloadProgressBlock;
@property (nonatomic, copy) LNTXUploadProgressBlock uploadProgressBlock;
@property (nonatomic, copy) LNTXBodyConstructionBlock bodyConstructionBlock;
@property (nonatomic) dispatch_queue_t completionQueue;
@property (nonatomic) dispatch_group_t completionGroup;
@property (nonatomic) NSURLSessionConfiguration *sessionConfiguration;
@property (nonatomic) BOOL shouldAcceptAllContentTypes;

@end

@interface LNTXWebServiceCallConfigurationBuilder : NSObject

+ (instancetype)builder;
- (LNTXWebServiceCallConfiguration *)build;
- (instancetype)setWebService:(LNTXWebService *)webService;
- (instancetype)setPathFormatValues:(NSDictionary *)pathFormatValues;
- (instancetype)setParameters:(id)parameters;
- (instancetype)setHTTPHeaders:(NSDictionary *)HTTPHeaders;
- (instancetype)setDownloadProgressBlock:(LNTXDownloadProgressBlock)downloadProgressBlock;
- (instancetype)setUploadProgressBlock:(LNTXUploadProgressBlock)uploadProgressBlock;
- (instancetype)setBodyConstructionBlock:(LNTXBodyConstructionBlock)bodyConstructionBlock;
- (instancetype)setCompletionQueue:(dispatch_queue_t)completionQueue;
- (instancetype)setCompletionGroup:(dispatch_group_t)completionGroup;
- (instancetype)setSessionConfiguration:(NSURLSessionConfiguration *)configuration;
- (instancetype)setShouldAcceptAllContentTypes:(BOOL)shouldAcceptAllContentTypes;

@end
