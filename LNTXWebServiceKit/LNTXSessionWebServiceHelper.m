//
//  LNTXSessionWebServiceHelper.m
//  LNTXWebServiceHelper
//
//  Created by Damien Rambout on 08/11/14.
//  Copyright (c) 2014 LINITIX. All rights reserved.
//

#import "LNTXSessionWebServiceHelper.h"

#import "LNTXJSONResponseSerializer.h"
#import "LNTXHTTPResponseSerializer.h"
#import "LNTXWebServiceCallConfiguration.h"
#import "LNTXWebService.h"
#import "LNTXWebServiceCompletion.h"
#import "LNTXWebServiceRequest.h"

static NSString * const kUnsupportedContentTypeException = @"LNTXUnsupportedContentTypeException";

typedef void(^LNTXHTTPSessionDataTaskCompletion)(LNTXWebServiceRequest *request, id responseObject, NSError *error);
typedef void(^AFHTTPSessionDataTaskSuccess)(NSURLSessionDataTask *dataTask, id responseObject);
typedef void(^AFHTTPSessionDataTaskFailure)(NSURLSessionDataTask *dataTask, NSError *error);

@interface LNTXSessionWebServiceHelper ()

@property (nonatomic) NSMutableArray *sessionManagers;

@end

@implementation LNTXSessionWebServiceHelper


- (instancetype)init {
    return [self initWithBaseURL:nil];
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    if (self = [super init]) {
        _baseURL = url;
        
        self.sessionManagers = [NSMutableArray arrayWithCapacity:5];
    }
    
    return self;
}

- (LNTXWebServiceRequest *)performHTTPCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration completion:(LNTXWebServiceCompletion *)completion {
    AFHTTPSessionManager *sessionManager = [self sessionManagerForCallConfiguration:configuration];
    sessionManager.responseSerializer = [LNTXHTTPResponseSerializer serializer];
    
    LNTXWebServiceRequest *request = [self performCallWithManager:sessionManager
                                                 forConfiguration:configuration
                                                       completion:^(LNTXWebServiceRequest *request,
                                                                    id responseObject,
                                                                    NSError *error) {
                                                           request.responseData = responseObject;
                                                           request.responseObject = responseObject;
                                                           [completion handleResult:responseObject request:request];
                                                           [self.sessionManagers removeObject:sessionManager];
                                                       }];
    
    return request;
}

- (LNTXWebServiceRequest *)performJSONCallWithConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                                 completion:(LNTXWebServiceCompletion *)completion {
    NSAssert(configuration.webService.parserConfiguration != nil,
             @"No parser configuration defined for web service %@. Define one to fetch JSON data.",
             configuration.webService);
    
    AFHTTPSessionManager *sessionManager = [self sessionManagerForCallConfiguration:configuration];
    sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    id <LNTXParserConfiguration> parserConfiguration = configuration.webService.parserConfiguration;
    sessionManager.responseSerializer = [LNTXJSONResponseSerializer serializerWithParserConfiguration:parserConfiguration];
    
    LNTXWebServiceRequest *request = [self performCallWithManager:sessionManager
                                                 forConfiguration:configuration
                                                       completion:^(LNTXWebServiceRequest *request,
                                                                    LNTXJSONResponseObject *responseObject,
                                                                    NSError *error) {
                                                           request.responseData = responseObject.data;
                                                           request.responseObject = responseObject.object;
                                                           [completion handleResult:responseObject.object request:request];
                                                           [self.sessionManagers removeObject:sessionManager];
                                                       }];
    
    return request;
}

- (AFHTTPSessionManager *)sessionManagerForCallConfiguration:(LNTXWebServiceCallConfiguration *)configuration {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:self.baseURL
                                           sessionConfiguration:configuration.sessionConfiguration];
    
    [self.sessionManagers addObject:manager];
    
    manager.completionGroup = configuration.completionGroup;
    manager.completionQueue = configuration.completionQueue;
    
    return manager;
}

- (LNTXWebServiceRequest *)performCallWithManager:(AFHTTPSessionManager *)manager
                                 forConfiguration:(LNTXWebServiceCallConfiguration *)configuration
                                       completion:(LNTXHTTPSessionDataTaskCompletion)completion {
    LNTXWebServiceRequest *request = [LNTXWebServiceRequest requestWithWebServiceCallConfiguration:configuration];
    NSURLSessionDataTask *dataTask = nil;
    
    // Prepare manager for request
    manager.requestSerializer = [self.class requestSerializerForContentType:configuration.webService.requestContentType];
    
    // Set HTTP headers
    [configuration.HTTPHeaders enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    
    NSString *path = [configuration formattedWebServicePath];
    
    switch (configuration.webService.HTTPMethod) {
        case LNTXHTTPMethodGET: {
            // GET
            dataTask = [manager GET:path
                      parameters:configuration.parameters
                         success:completionToSuccess(completion, request)
                         failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodPOST: {
            // POST
            if (configuration.bodyConstructionBlock) {
                // Multipart
                dataTask = [manager POST:path
                           parameters:configuration.parameters
            constructingBodyWithBlock:configuration.bodyConstructionBlock
                              success:completionToSuccess(completion, request)
                              failure:completionToFailure(completion, request)];
            } else {
                // Regular
                dataTask = [manager POST:path
                           parameters:configuration.parameters
                              success:completionToSuccess(completion, request)
                              failure:completionToFailure(completion, request)];
            }
            
            break;
        }
            
        case LNTXHTTPMethodPUT: {
            // PUT
            dataTask = [manager PUT:path
                      parameters:configuration.parameters
                         success:completionToSuccess(completion, request)
                         failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodDELETE: {
            // DELETE
            dataTask = [manager DELETE:path
                         parameters:configuration.parameters
                            success:completionToSuccess(completion, request)
                            failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodPATCH: {
            // PATCH
            dataTask = [manager PATCH:path
                        parameters:configuration.parameters
                           success:completionToSuccess(completion, request)
                           failure:completionToFailure(completion, request)];
            break;
        }
            
        case LNTXHTTPMethodHEAD: {
            // HEAD
            dataTask = [manager HEAD:path
                       parameters:configuration.parameters
                          success:^(NSURLSessionDataTask *dataTask) {
                              completion(request, nil, nil);
                          }
                          failure:completionToFailure(completion, request)];
        }
    }
    
    request.URLSessionDataTask = dataTask;
    
    return request;
}

+ (AFHTTPRequestSerializer *)requestSerializerForContentType:(NSString *)contentType {
    if ([contentType isEqualToString:LNTXContentTypeJSON]) {
        return [AFJSONRequestSerializer serializer];
    }
    
    if (!contentType ||
        [contentType isEqualToString:LNTXContentTypeFormURLEncoded] ||
        [contentType hasPrefix:@"multipart/"]) {
        return [AFHTTPRequestSerializer serializer];
    }
    
    [NSException raise:kUnsupportedContentTypeException
                format:@"Content-type '%@' is not support supported by class %@.", contentType, NSStringFromClass(self.class)];
    return nil;
}

#pragma mark - Helper methods

static AFHTTPSessionDataTaskSuccess completionToSuccess(LNTXHTTPSessionDataTaskCompletion completion, LNTXWebServiceRequest *request) {
    return ^(NSURLSessionDataTask *dataTask, id responseObject) {
        completion(request, responseObject, nil);
    };
}

static AFHTTPSessionDataTaskFailure completionToFailure(LNTXHTTPSessionDataTaskCompletion completion, LNTXWebServiceRequest *request) {
    return ^(NSURLSessionDataTask *dataTask, NSError *error) {
        request.error = error;
        
        id responseObject = nil;
        if (error) {
            responseObject = error.userInfo[LNTXRequestResponseObjectUserInfoKey];
        }
        
        // TODO: Response object anyway
        completion(request, responseObject, error);
    };
}

@end
