Pod::Spec.new do |s|
  s.name         = "LNTXWebServiceKit"
  s.version      = "0.5.0"
  s.summary      = "LNTXWebServiceKit provides tools to easily communicate with remote web services."
  s.homepage     = "http://www.linitix.com"
  s.license      = 'MIT'
  s.author       = { "Damien Rambout" => "damien.rambout@linitix.com" }
  s.source       = { :git => "https://bitbucket.org/linitixios/lntxwebservicehelper.git", :tag => s.version }

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  
  s.ios.exclude_files = '**/*.pch'

  s.source_files = 'LNTXWebServiceKit'
  
  s.dependency 'LNTXCoreKit', '~> 0.5'
  s.dependency 'LNTXParser', '~> 0.2'
  s.dependency 'AFNetworking', '~> 2.5'
end
